<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\LogFaceRecognition;

class LogUploadFaceRecognition extends Command
{
    protected $signature = 'log:upload-facerecognition {userId} {imagePath}';
    protected $description = 'Log face recognition event for a user';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $userId = $this->argument('userId');
        $imagePath = $this->argument('imagePath');

        // dd('app/public/' . $imagePath);
        $base64Image = base64_encode(file_get_contents(storage_path('app/public/' . $imagePath)));

        $log = LogFaceRecognition::updateOrCreate(
            ['user_id' => $userId],
            ['image' => $base64Image]
        );

        $message = $log->wasRecentlyCreated ?
            'Face recognition event logged for user ID: ' . $userId :
            'Face recognition event for user ID: ' . $userId . ' already logged.';

        $this->info($message);
    }
}
