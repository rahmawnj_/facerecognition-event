<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PhpMqtt\Client\MqttClient;
use Illuminate\Support\Facades\Log;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use PhpMqtt\Client\Exceptions\MqttClientException;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;

class Controller extends BaseController
{
    use AuthorizesRequests, ValidatesRequests;

    public function receiveHeartbeat(Request $request)
    {
        $data = $request->all();

        $timestamp = now()->format('d-m-Y H:i:s');

        $payload = [
            'operator' => $data['operator'],
            'info' => $data['info'],
            'timestamp' => $timestamp
        ];

        $this->publishToMqtt($payload, 'fc-event-status');

        $response = Http::get('http://192.168.99.116:8001/api/log-facerecognitions');
        if ($response->successful() && $response->json()) {
            $this->postToAddPerson($response->json());
        }
        return response()->json(['message' => 'Heartbeat received and published to MQTT.']);
    }

    private function publishToMqtt($data, $topic)
    {
        $server = env('MQTT_SERVER', 'broker.emqx.io');
        $port = env('MQTT_PORT', 1883);
        $clientId = Str::random(10);

        try {
            $mqtt = new MqttClient($server, $port, $clientId);
            $mqtt->connect();

            $mqtt->publish($topic, json_encode($data), 0);
            Log::info('Data published to ' . $topic);

            $mqtt->disconnect();
        } catch (MqttClientException $e) {
            Log::error('Error in MQTT operation: ' . $e->getMessage());
        }
    }

    private function postToAddPerson($data)
    {
        $deviceTarget = [
            'username' => 'admin',
            'password' => 'admin',
            'ip_address' => 'http://192.168.99.114'
        ];
        $userData = [
            "operator" => "AddPerson",
            "info" => [
                "DeviceID" => 2400110,
                "PersonType" => 0,
                "Name" => $data['user']['name'],
                "IdCard" => $data['user']['id'],
                "Telnum" => $data['user']['phone_number'],
                "Address" => $data['user']['address'],
                "CustomizeID" => $data['user']['id'],
                "PersonUUID" => $data['user']['id']
            ],
            "picinfo" => "data:image/jpeg;base64," . $data['image']
        ];

        $json = json_encode($userData, JSON_UNESCAPED_SLASHES);

        $headers = [
            "Authorization: Basic " . base64_encode("{$deviceTarget['username']}:{$deviceTarget['password']}"),
            'Content-Type: application/x-www-form-urlencoded'
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $deviceTarget['ip_address'] . '/action/AddPerson');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = json_decode(curl_exec($ch), true);
        curl_close($ch);

        $res = [
            'info' => $result['info'],
            'user_id' => $data['user']['id']
        ];

        $this->publishToMqtt($res, 'fc-event-response-' . $data['user']['id']);

        if ($result['code'] == 200) {
            Log::info(json_encode($result));
        } else {
            Log::error(json_encode($result));

            if ($result['info']['Result'] == 'Fail') {
                $userData['operator'] = 'EditPerson';
                $userData['info']['IdType'] = 0;

                $json = json_encode($userData, JSON_UNESCAPED_SLASHES);

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $deviceTarget['ip_address'] . '/action/EditPerson');
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $editResult = json_decode(curl_exec($ch), true);
                curl_close($ch);

                $res = [
                    'info' => $editResult['info'],
                    'user_id' => $data['user']['id']
                ];

                $this->publishToMqtt($res, 'fc-event-response-' . $data['user']['id']);

                if ($editResult['code'] == 200) {
                    Log::info(json_encode($editResult));
                } else {
                    Log::error(json_encode($editResult));
                }
            }
        }
    }
}
