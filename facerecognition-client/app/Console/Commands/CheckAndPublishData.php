<?php

namespace App\Console\Commands;

use PhpMqtt\Client\MqttClient;
use Illuminate\Console\Command;
use App\Models\LogFacerecognition;
use Illuminate\Support\Facades\Log;

class CheckAndPublishData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:check-and-publish';
    protected $description = 'Check data and publish if available';

    public function handle()
    {
        try {
            $latestData = LogFacerecognition::latest()->first();
            // dd($latestData);
            if ($latestData) {
                $this->publishData($latestData);
            } else {
                $this->info('No data found.');
            }
        } catch (\Exception $e) {
            Log::error('Error checking and publishing data: ' . $e->getMessage());
            $this->error('An error occurred while checking and publishing data.');
        }
    }

    protected function publishData($data)
    {
        $server = env('MQTT_SERVER', 'broker.emqx.io');
        $port = env('MQTT_PORT', 1883);
        $username = env('MQTT_USERNAME', '');
        $password = env('MQTT_PASSWORD', '');
        $clientId = env('MQTT_CLIENT_ID', 'fc-event-data') . rand(1000, 9999);

        $mqtt = new MqttClient($server, $port, $clientId);
        $mqtt->connect();

        $tes = $mqtt->publish('e-log-facerecognition', json_encode([
            'user_id' => $data->user_id,
            'image' => $data->image,

        ]), 0);

        $this->info('Data published successfully.');

        $data->delete();
        $mqtt->disconnect();
    }
}
