<div id="mqtt-status-wrapper" style="position: fixed; bottom: 10%; right: 2%; z-index: 999;">
    <div style="background-color: #fff; padding: 10px; border-radius: 8px; box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);">
        <script src="{{ asset('assets/js/paho-mqtt.js') }}"></script>
        <script>
            document.addEventListener('DOMContentLoaded', function() {
                const mqttServer = '{{ env('MQTT_SERVER', 'broker.emqx.io') }}';
                const mqttPort = {{ env('MQTT_WS_PORT', 8083) }};
                const clientId = 'local-' + Math.random();
                const statusTopic = 'fc-event-status';
                const responseTopic = 'fc-event-response-'+ '{{ auth()->user()->id }}';

                const client = new Paho.MQTT.Client(mqttServer, mqttPort, clientId);
                let lastReceivedStatusTimestamp = null;
                let lastReceivedResponseTimestamp = null;

                client.onConnectionLost = function(responseObject) {
                    if (responseObject.errorCode !== 0) {
                        console.log('Koneksi terputus: ' + responseObject.errorMessage);
                    }
                    setStatus('Offline');
                    setIndicatorColor('red');
                };

                client.onMessageArrived = function(message) {
                    const payload = JSON.parse(message.payloadString);
                    if (message.destinationName === statusTopic) {
                        handleStatusMessage(payload);
                    } else if (message.destinationName === responseTopic) {
                        handleResponseMessage(payload);
                    }
                };

                client.connect({
                    onSuccess: function() {
                        console.log('Terhubung ke MQTT');
                        client.subscribe(statusTopic);
                        client.subscribe(responseTopic);
                        setStatus('Online');
                        setIndicatorColor('green');

                        checkOfflineStatus();
                        setInterval(checkOfflineStatus, 1000);
                    },
                    onFailure: function(message) {
                        console.log('Gagal terhubung ke MQTT: ' + message.errorMessage);
                        setStatus('Offline');
                        setIndicatorColor('red');
                    }
                });

                const handleStatusMessage = (payload) => {
                    const [day, month, year, hour, minute, second] = payload.timestamp.split(/[- :]/);
                    const receivedTimestamp = new Date(year, month - 1, day, hour, minute, second);

                    const now = new Date();
                    const elapsedTimeInSeconds = (now - receivedTimestamp) / 1000;

                    if (elapsedTimeInSeconds < 7) {
                        setStatus('Online');
                        setIndicatorColor('green');
                    } else {
                        setStatus('Offline');
                        setIndicatorColor('red');
                    }

                    lastReceivedStatusTimestamp = receivedTimestamp;
                };

                const handleResponseMessage = (payload) => {
                    alert(payload['info']['Result '])
                    console.log('Received response message:', payload);
                };

                const checkOfflineStatus = () => {
                    const now = new Date();
                    const elapsedTimeInSeconds = (now - lastReceivedStatusTimestamp) / 1000;

                    if (elapsedTimeInSeconds > 20) {
                        setStatus('Offline');
                        setIndicatorColor('red');
                    }
                };

                const setStatus = (status) => {
                    document.getElementById('mqtt-status').innerText = status;
                };

                const setIndicatorColor = (color) => {
                    document.getElementById('status-indicator').style.backgroundColor = color;
                };

            });
        </script>

        <div style="display: flex; align-items: center;">
            <div id="status-indicator"
                style="width: 10px; height: 10px; border-radius: 50%; margin-right: 5px; background-color: red;"></div>
            <div id="mqtt-status">Offline</div> <!-- Set initial status to Offline -->
        </div>
    </div>
</div>
