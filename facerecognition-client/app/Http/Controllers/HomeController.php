<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\LogFacerecognition;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Storage;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function image_upload(Request $request)
    {
        $request->validate([
            'profile_image' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048', // sesuaikan dengan kebutuhan
        ]);

        $imagePath = $request->file('profile_image')->store('images/users', 'public');
        $oldImagePath = Auth::user()->image;

        Auth::user()->update([
            'image' => Storage::url($imagePath),
        ]);

        if ($oldImagePath) {
            $oldImagePath = str_replace('/storage', 'public', $oldImagePath);
            Storage::delete($oldImagePath);
        }
        // die;

        Artisan::call('log:upload-facerecognition', [
            'userId' => Auth::id(),
            'imagePath' => $imagePath,
        ]);

        return redirect()->back()->with('success', 'Profile image updated successfully');
    }

    public function image_export(Request $request)
    {
        $user = Auth::user();
        $imagePath = str_replace('/storage/', '', $user->image);

        Artisan::call('log:upload-facerecognition', [
            'userId' => $user->id,
            'imagePath' => $imagePath,
        ]);

        return redirect()->back()->with('success', 'Profile exported successfully');
    }

    public function getLogFacerecognition()
    {
        $data = LogFacerecognition::with('user')->latest()->first();

        if ($data) {
            $data->delete();
        }

        return response()->json($data);
    }

}
