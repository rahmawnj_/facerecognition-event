<?php

require __DIR__.'/vendor/autoload.php'; // Sesuaikan dengan lokasi autoload.php Anda
require __DIR__.'/bootstrap/app.php'; // Sesuaikan dengan lokasi bootstrap app.php Anda

use Illuminate\Support\Facades\Artisan;

Artisan::call('mqtt:check-and-publish');
