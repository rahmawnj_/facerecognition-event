<div class="card">
    <div class="card-header d-flex justify-content-between align-items-center">
        <span>{{ __('Dashboard') }}</span>
        <a href="{{ route('user.profile', ['id' => Auth::user()->id]) }}" class="btn btn-primary">{{ __('View Profile') }}</a>
    </div>
    <div class="card-body">
        @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
        @endif
        <div class="text-center">
            <h4>{{ Auth::user()->name }}</h4>
            <p class="text-muted">{{ Auth::user()->email }}</p>
        </div>

        <div class="mb-3 text-center">
            <img src="{{ asset(Auth::user()->image) }}" class="img-fluid rounded-circle" alt="Profile Image" style="width: 150px; height: 150px;">
        </div>

        <div class="text-center">
            <form method="POST" action="{{ route('user.updateProfileImage') }}" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="imageSource" id="uploadFile" value="file" checked>
                        <label class="form-check-label" for="uploadFile">Upload from File</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="imageSource" id="captureCamera" value="camera">
                        <label class="form-check-label" for="captureCamera">Capture with Camera</label>
                    </div>
                </div>

                <div id="fileInput" class="form-group">
                    <input type="file" name="profile_image" class="form-control-file">
                </div>

                <div id="cameraInput" class="form-group d-none text-center">
                    <button type="button" class="btn btn-secondary" onclick="startCamera()">Open Camera</button>
                    <video id="cameraPreview" width="320" height="240" autoplay class="d-none"></video>
                    <canvas id="cameraCanvas" class="d-none"></canvas>
                    <input type="hidden" name="camera_image" id="cameraImage">
                </div>

                <button type="submit" class="btn btn-success mt-3">Update Profile Image</button>
            </form>
        </div>
    </div>
</div>

<script>
    document.addEventListener('DOMContentLoaded', function() {
        const uploadFile = document.getElementById('uploadFile');
        const captureCamera = document.getElementById('captureCamera');
        const fileInput = document.getElementById('fileInput');
        const cameraInput = document.getElementById('cameraInput');
        const cameraPreview = document.getElementById('cameraPreview');
        const cameraCanvas = document.getElementById('cameraCanvas');
        const cameraImage = document.getElementById('cameraImage');

        uploadFile.addEventListener('change', function() {
            fileInput.classList.remove('d-none');
            cameraInput.classList.add('d-none');
        });

        captureCamera.addEventListener('change', function() {
            fileInput.classList.add('d-none');
            cameraInput.classList.remove('d-none');
        });
    });

    function startCamera() {
        const cameraPreview = document.getElementById('cameraPreview');
        const cameraCanvas = document.getElementById('cameraCanvas');
        const cameraImage = document.getElementById('cameraImage');

        if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
            navigator.mediaDevices.getUserMedia({
                    video: true
                })
                .then(stream => {
                    cameraPreview.srcObject = stream;
                    cameraPreview.classList.remove('d-none');
                    const track = stream.getTracks()[0];

                    cameraPreview.addEventListener('canplay', () => {
                        cameraCanvas.width = cameraPreview.videoWidth;
                        cameraCanvas.height = cameraPreview.videoHeight;
                    });

                    cameraPreview.addEventListener('click', () => {
                        cameraCanvas.getContext('2d').drawImage(cameraPreview, 0, 0);
                        const dataURL = cameraCanvas.toDataURL('image/png');
                        cameraImage.value = dataURL;
                        track.stop(); // Stop the camera
                        cameraPreview.classList.add('d-none');
                        cameraCanvas.classList.remove('d-none');
                    });
                })
                .catch(err => {
                    alert('Unable to access the camera. Please allow camera access in your browser settings.');
                    console.error('Error accessing camera: ', err);
                });
        } else {
            alert('Camera API is not supported in your browser.');
        }
    }
</script>
