@extends('layouts.app')

@section('content')
@include('components.indicator')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header d-flex justify-content-between align-items-center">
                        <span>{{ __('Dashboard') }}</span>
                        <form action="{{ route('image.export') }}" method="POST">
                            @csrf
                            <button type="submit" class="btn btn-sm btn-warning">{{ __('Export') }}</button>
                        </form>
                    </div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <div class="text-center">
                            <h4>{{ Auth::user()->name }}</h4>
                            <p class="text-muted">{{ Auth::user()->email }}</p>
                        </div>

                        <div class="mb-3 text-center">
                            <img id="previewImage" src="{{ asset(Auth::user()->image) }}" class="img-fluid rounded-circle"
                                alt="Profile Image" style="width: 150px; height: 150px;">
                        </div>

                        <div class="text-center">
                            <form method="POST" action="{{ route('image.upload') }}" enctype="multipart/form-data">
                                @csrf
                                @method('PUT')
                                <div class="form-group">
                                    <input type="file" @required(true) accept="image/*" name="profile_image" class="form-control-file"
                                        id="profileImageInput">
                                </div>

                                <button type="submit" class="btn btn-success mt-3">Update Profile Image</button>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <script>
        document.addEventListener('DOMContentLoaded', function() {
            const profileImageInput = document.getElementById('profileImageInput');
            const previewImage = document.getElementById('previewImage');

            profileImageInput.addEventListener('change', function(event) {
                const file = event.target.files[0];
                if (file) {
                    const reader = new FileReader();
                    reader.onload = function(e) {
                        previewImage.src = e.target.result;
                    };
                    reader.readAsDataURL(file);
                }
            });
        });
    </script>
@endsection
