<?php

namespace App\Console\Commands;

use PhpMqtt\Client\MqttClient;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Http;
use PhpMqtt\Client\Exceptions\MqttClientException;

class PublishStatus extends Command
{
    protected $signature = 'mqtt:publish-status';
    protected $description = 'Publish online status to MQTT broker';


    public function handle()
    {
        $server = env('MQTT_SERVER', 'broker.emqx.io');
        $port = env('MQTT_PORT', 1883);
        $username = env('MQTT_USERNAME', '');
        $password = env('MQTT_PASSWORD', '');
        $clientId = env('MQTT_CLIENT_ID', 'local-project') . rand(1000, 9999);

        try {
            $response = Http::get('http://192.168.99.116:8001/api/log-facerecognitions');

            if ($response->successful()) {
                $data = $response->json();
                Log::info($data);

                $mqtt = new MqttClient($server, $port, $clientId);
                $mqtt->connect();
                $mqtt->publish('fc-event-status', json_encode(['timestamp' => now()->format('d-m-Y H:i:s')]), 0);
                $this->info('Data published to MQTT');
                $mqtt->disconnect();
            } else {
                $this->error('Failed to fetch data from API.');
            }
        } catch (\Exception $e) {
            Log::error('Error fetching data from API or publishing to MQTT: ' . $e->getMessage());
            $this->error('An error occurred while fetching data from API or publishing to MQTT.');
        }
    }
}
